package com.agileengine;

import org.openqa.selenium.By;
import org.openqa.selenium.InvalidSelectorException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        String inputPath = args[0];
        String searchingValue = "everything ok";

        // Declaring and initialising the HtmlUnitWebDriver
        HtmlUnitDriver unitDriver = new HtmlUnitDriver();

        // Necessary set Proxy if you're behind it !!!!
//        unitDriver.setProxy("proxy.YOUR-ORGANIZATION.COM", XXXX);

        unitDriver.get(inputPath);

        Optional <WebElement> optional = unitDriver
                .findElements(By.xpath("//*[contains(translate(text(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'), '"+ searchingValue +"')]"))
                .stream()
                .filter(e -> e.getAttribute("class").equals("btn btn-success") &&
                            !e.getText().isEmpty())
                .findFirst();
        if (optional.isPresent()) {
            WebElement finding = optional.get();
            String absolutePath = attributePath(finding, "");
            WebElement parentOfParentElement = finding.findElement(By.xpath("./.."));
            absolutePath =  attributePath(parentOfParentElement, absolutePath);
            boolean whileNotAPage = true;
            while (whileNotAPage) {
                try {
                    parentOfParentElement = parentOfParentElement.findElement(By.xpath("./.."));
                    absolutePath = attributePath(parentOfParentElement, absolutePath);
                } catch (InvalidSelectorException e) {
                    whileNotAPage = false;
                }
            }
            System.out.println("Smile:) The element that you are looking for in => *'" + absolutePath +"'*");
        } else {
            System.out.println("No searching element: " + searchingValue);
        }
    }

    private static String attributePath(WebElement parentOfParentElement, String absolutePath) {
        return parentOfParentElement.getTagName() + "(id: " + parentOfParentElement.getAttribute("id") + "; class: " + parentOfParentElement.getAttribute("class") + ")" + " > " + absolutePath;
    }
}
